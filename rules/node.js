const node = require('eslint-config-airbnb-base/rules/node')

node.rules['node/no-unsupported-features'] = 'off'

// eslint-disable-next-line prefer-object-spread
module.exports = Object.assign({}, node, {
  plugins: ['node'],
  'extends': ['plugin:node/recommended'],
  env: {
    commonjs: true,
  },
})
