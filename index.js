module.exports = {
  'extends': [
    './rules/node',
  ]
    .map(require.resolve),
  plugins: [],
  parser: 'babel-eslint',
  rules: {},
}
